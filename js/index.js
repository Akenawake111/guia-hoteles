$(function(){
    		$("[data-toggle='tooltip']").tooltip();
    		$("[data-toggle='popover']").popover();
    		//El intervalo entre transiciones del acrrousel se puede modificar. al igual  que muchas cosas. 
    		$('.carousel').carousel({
    			interval:500
    		});

    		//MODAL DEL ULTIMO BOTON
    		//show al abrir shown al cerrar
    		//
    		$('#contacto').on('show.bs.modal', function (e){
    			console.log('El modal se esta mostrando');
    			$('#btnmodal').removeClass('btn-outline-success');
    			$('#btnmodal').addClass('btn-primary');
    			$('#btnmodal').prop('disabled',true);
    		});
    		$('#contacto').on('shown.bs.modal', function (e){
    			console.log('El modal se mostro');
    		});
    		$('#contacto').on('hide.bs.modal', function (e){
    			console.log('El modal se oculta');
    		});
    		$('#contacto').on('hidden.bs.modal', function (e){
    			console.log('El modal se esta oculto');
    			$('#btnmodal').removeClass('btn-primary');
    			$('#btnmodal').addClass('btn-outline-success');
    			$('#btnmodal').prop('disabled',false);
    		});


    	});